<?php
//require_once MODX_CORE_PATH.'components/myactionsoffer/processors/report.class.php';
require_once dirname(__FILE__)."/index.class.php";

require_once dirname(dirname(__FILE__)) . '/model/sheets.class.php';

//core/components/translit/model/modx/translit/tables/russian.php

class ProductsImportProcessor extends loadOffersProcessor {  

    public $modx = null;
    public $config = array();
   
    function __construct(modX &$modx,array $config = array()) {
        return parent::__construct($modx, $config);
    }

    public function process() { 
        //$context = $this->config['context'];
        $context = 'web';
        $data=$this->importProducts($context,'more');
        $output = array(
            'success' => true,
            'message' => 'test',
            'object' => $data
        );      
        return $output;        
    }

    
    public function importProducts($context,$option){ 
        $values = new Sheets;
        $values = $values->readData($option);

        # отбираем только нужные поля   
        $fields_array = array();
        $tvs_array = array();    
        foreach($values as $k=>$v){ 
          if($k==0){         
            $fields_array[]=array_intersect($v, array_keys($this->config['fields'])); 
            $tvs_array[]=array_intersect($v, array_keys($this->config['tvs'])); 
          } 
        }  
        
        //$this->Logs('fields_array',$fields_array,$context);

        # 1) Находим заголовки    
        $headings=array();
        foreach ($values as $key=>$value){
          if($key==0){
            $headings[]=$value;
          } 
        } 
        $headings=array_values($headings[0]);      

        # 2) Компилируем в вид: array('id'=>'339287','pagetitle'=>'букет такой-то') 
        $counter=0; 
        $error='';

        foreach ($values as $key=>$value){ #один Оффер
          if($key==0) continue; # Заголовок      
          foreach ($value as $j => $item){ #данные одного Оффера    
            $dataArray[$key][$headings[$j]]=$item;               
          } 
       
          $e = $this->createProduct($key,$dataArray,$fields_array[0],$tvs_array[0],$context);          
        
          if($e){
                $error.= "<p>".$e."</p>";
            }else{
                $counter++;
            }
        }  
        //$this->Logs('goods',$dataArray,$context);        
        return $error;
    }
    
    
    
    public function transliterate($in){
        $gost = array(
            "а" => "a",   "б" => "b",   "в" => "v",
            "г" => "g",   "д" => "d",   "е" => "e",
            "ё" => "e",   "ж" => "zh",  "з" => "z",
            "и" => "i",   "й" => "y",   "к" => "k",
            "л" => "l",   "м" => "m",   "н" => "n",
            "о" => "o",   "п" => "p",   "р" => "r",
            "с" => "s",   "т" => "t",   "у" => "u",
            "ф" => "f",   "х" => "h",   "ц" => "c",
            "ч" => "ch",  "ш" => "sh",  "щ" => "sch",
            "ь" => "",  "ы" => "y",   "ъ" => "",
            "э" => "e",   "ю" => "yu",  "я" => "ya",
            "А" => "a",   "Б" => "b",   "В" => "v",
            "Г" => "g",   "Д" => "d",   "Е" => "e",
            "Ё" => "e",   "Ж" => "zh",  "З" => "z",
            "И" => "i",   "Й" => "y",   "К" => "k",
            "Л" => "l",   "М" => "m",   "Н" => "n",
            "О" => "o",   "П" => "p",   "Р" => "r",
            "С" => "s",   "Т" => "t",   "У" => "u",
            "Ф" => "f",   "Х" => "h",   "Ц" => "c",
            "Ч" => "ch",  "Ш" => "sh",  "Щ" => "sch",
            "Ь" => "",  "Ы" => "y",   "Ъ" => "",
            "Э" => "e",   "Ю" => "yu",  "Я" => "ya", "," => "", " " => "-", "/" => "", "(" => "", ")" => "", "," => "-","&amp" =>"","&" =>"","!" =>"","+" =>""
        );
        return strtr($in, $gost);
    }


    public function createProduct($key,$data,$fields_array,$tvs_array,$context){
        $code = false;
        $e = '';  

        if(!$data[$key]['category']) $e .= '<strong>отсуствует айди магазина родителя</strong><br>';
        /*
        if(isset($data[$key]['shop_title']){
            $data[$key]['shop_title']
        }
        */
        if(isset($data[$key]['id'])){
            $codedata = array();
            
            foreach($fields_array as $k=>$v){
              $codedata[$key][$v]=$data[$key][$v]; 
            }
            
            $code = $this->modx->getObject('modResource',$data[$key]['id']);  
           
            if(!$code){
        		$code = $this->modx->newObject('modResource');
                $catObj = $this->modx->getObject('modResource',array('alias'=>$codedata[$key]['category'])); 
     
                if(is_object($catObj)){
                    //$codedata[$key]['context_key'] = $context;
                    $codedata[$key]['parent'] = $catObj->get('id');
                    $codedata[$key]['template'] = $this->config['template'];
                    $codedata[$key]['published'] = 1;
                    //$codedata[$key]['createdon'] = time();  
                    //$codedata[$key]['publishedon'] = strtotime("now"); ('createdon', time());  
                    $codedata[$key]['alias'] = $this->transliterate($data[$key]['pagetitle']);
                    
                    $e .= '<strong>Новый ресурс в каталоге '.$codedata[$key]['category'].'</strong><br>';
                }else{
                    $e .= '<strong>Нет категории с алиасом: '.$codedata[$key]['category'].'</strong><br>'; 
                }
        	}
            
            # -=-=-=--=-=-=-=-=-=-=--=-- Обновляем ресурс
            if($codedata){ 
              $code->fromArray($codedata[$key]);
              // ---- akustik! $code->save();                
            } 
            
            # -=-=-=--=-=-=-=-=-=-=--=-- обновляем поля
            $t=array();
            foreach($tvs_array as $k=>$v){  
                $tvVal = $data[$key][$v]; 
               
                if(!empty($tvVal) && $v=='test'){
                    $tmptvVal=explode("\n",$tvVal);
                    foreach($tmptvVal as $rtv) {
                    	$ou[]=explode('&',$rtv); 
                    }
                    if(is_array($ou)){
                        $i=1;
                        //$pusher = array();
                    	foreach($ou as $ktv => $vtv){
                    	    
                    	    $pusher[] = array( 
                               //"MIGX_id"=>'"'.$i.'"', //$ktv+1
                               "test1"=>$vtv[0],
                               "pricefix1"=>$vtv[1],
                            );
                           
                            //----------------new $pusher[$ktv]['test1'] = $vtv[0]; $pusher[$ktv]['pricefix1'] = $vtv[1];
                            //$pusher[$ktv] = $vtv[$ktv];
                           
                            
                            
                            if($ktv=='test1'){ 
                               //$pusher['test1'] = $vtv[0];
                                //$this->Logs('test1vtv',$vtv,$context);
                            } 
                            if($ktv=='pricefix1'){ 
                               //$pusher['pricefix1'] = $vtv[1];
                                //$this->Logs('pricefix1',$vtv,$context);
                            } 
                             //---------------- end new
                            $tojson[] = $pusher;
                            $i++;
                    	}
                        // akustik ---------------- !!!!!       $tvVal = json_encode($tojson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        # $tvVal = json_encode($tojson, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                        
                        //---- new---
                         $this->Logs('updatedJson',$pusher,$context);
                        
                        $oldTvJsonVal = $this->modx->fromJSON($code->getTVValue($v));
                        //$resourceColumns = array_flip(array_intersect_key('test1','pricefix1')); 
                        
                        
                        foreach($oldTvJsonVal as $i => $oldTvJsonValItm){
                            //$resourceColumns[] = array_keys($oldTvJsonValItm);
                            
                            //if($oldTvJsonValItm[$i] == 'test1'){
                                //$this->Logs('oldTvJsonVal',$oldTvJsonValItm['test1'],$context);
                                //$this->Logs('oldTvJsonVal',$oldTvJsonValItm['pricefix1'],$context);
                                //$this->Logs('oldTvJsonVal',$pusher[$i]['test1'],$context);
                             
                                $oldTvJsonValItm['test1'] = $pusher[$i]['test1'];
                                $oldTvJsonValItm['pricefix1'] = $pusher[$i]['pricefix1'];
                                //array_push($stack, "apple", "raspberry");
                                /*
                                $pusherto[] = array( 
                                   //"MIGX_id"=>'"'.$i.'"', //$ktv+1
                                   "test1"=>$vtv[0],
                                   "pricefix1"=>$vtv[1],
                                );
                                */
                                
                            //}
                            //$resourceColumns = array_flip(array_intersect_key($header,$obj)); 
                            //$tvColumns  = array_flip(array_diff_key($header,$obj));
                            //$fie[]=array_intersect($oldTvJsonValItm, array_keys('test1','pricefix1')); 
                            
                        }
                        
                        $eee = array_replace_recursive($oldTvJsonVal,$pusher);
                        
                        $this->Logs('oldTvJsonVal',$eee,$context);
                        
                        
                        /*
                        $base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
                        $replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));
                        $basket = array_replace_recursive($base, $replacements);
                        print_r($basket);
                        */
                        
                        
                        
                        
                        
                        $updatedJson = $tojson;
                        //$tvVal = json_encode($updatedJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        //$this->Logs('updatedJson',$updatedJson,$context);
                        
                    }
                }
                 
                 
                
                
                /*
                if(!empty($tvVal) && $v=='test'){
                    $tmptvVal=explode("\n",$tvVal);
                    foreach($tmptvVal as $rtv) {
                    	$ou[]=explode('&',$rtv); 
                    }
                    if(is_array($ou)){
                        $i=1;
                    	foreach($ou as $ktv => $vtv){
                    	    $pusher = array( 
                               "MIGX_id"=>'"'.$i.'"', //$ktv+1
                               "title"=>$vtv[0],
                               "image"=>$vtv[1],
                            );
                            $tojson[] = $pusher;
                            $i++;
                    	}
                        $tvVal = json_encode($tojson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        # $tvVal = json_encode($tojson, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                    }
                }
                */
                /*
                $json = '{
                    "id":"83dc1bd25f0487ae32603a1a8b304690",
                    "response-code":"200",
                    "title":"My Document Title",
                    "content":"My document content."
                }';
                $array = $modx->fromJSON($json);
                return $modx->getChunk('myDocTpl', $array);
                */
                
                
                
                if($code->getTVValue($v,$tvVal) != $data[$key][$v]){ // TO DO
                    $t[]=$data[$key][$v]; 
                }  
                $t[]=$data[$key][$v]; # Данные для вывода в лог
                
                // ---- akustik! $code->setTVValue($v,$tvVal); 
                
                # для price_final - Плагин для обновления поля при сохранении
                /*
                $this->modx->invokeEvent('OnDocFormSave',array(
                    'mode' => modSystemEvent::MODE_UPD,
                    'id' => $code->get('id'),
                    'resource' => &$code,
                    'object' => &$code,
                ));
                */
                
            }
            # $e .= 'Апдейт: '.$data[$key]['category'].'; id:'.$data[$key]['id'].'; tvs:['.implode('; ',$t).']';
            $e .= 'Апдейт: '.$data[$key]['category'].'; id:'.$data[$key]['id']; 
            # $this->modx->cacheManager->clearCache(); 
        }
        return $e;       
    }

    #################################### IMPORT (END) ##########################################      
}
return 'ProductsImportProcessor';