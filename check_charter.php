<?php
if($_POST['type'] == 'charter') {
    $post = [
        'origin' => $_POST['from'],
        'destination' => $_POST['to'],
        'flight_type' => 'charter',
        'flight_date' => $_POST['date'],
        'name' => $_POST['firstname'],
        'surname' => $_POST['lastname']
    ];
}else if ($_POST['type'] == 'regular'){
    $post = [
        'flight_type' => 'regular',
        'ticket_number' => $_POST['ticket_num'],
    ];
}else{
    echo json_encode([
        'success' => false,
        'message' => 'Unknown'
    ]);

    die();
}
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://paxinform.flyuia.com:1122/pax-check/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($post),
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer 638a71742c654acb9ef37e64080454935802bdf8',
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
    curl_close($curl);

    if ($code == 200) {
        echo $response;
    } else if ($code == 204) {
        echo $response;
    } else if ($code == 400) {
        echo $response;
    } else {
        echo json_encode([
            'success' => false,
            'message' => 'Unknown'
        ]);
    }


